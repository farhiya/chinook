echo "Please enter your ip address: "

read ip_address

COUNTER=0
for arg in $ip_address
do
COUNTER=$(expr $COUNTER + 1)
done

if ((COUNTER == 1))
then 
    echo "This is your ip $arg" 

elif ((COUNTER < 1))
then
    echo "There was no input. Try again! "
    
    # `exit 1` will stop bash script
    exit 1
elif ((COUNTER > 1))
then

    echo "That was too many inputs. Try again! "

    exit 1

fi


# opening instance and removing fingerprint check at given ip
ssh -o StrictHostKeyChecking=no -i ~/.ssh/farhiya-first-aws-key.pem ec2-user@$ip_address '
# update packages in ec2
sudo yum update -y
# install mariadb
sudo yum install mariadb-server -y
# start maria db
sudo systemctl enable mariadb
sudo systemctl start mariadb
sudo systemctl status mariadb

sudo yum istall wget
wget https://raw.githubusercontent.com/cwoodruff/ChinookDatabase/master/Scripts/Chinook_MySql.sql
mysql -u root --password="" -e "source Chinook_MySql.sql"
mysql -u root --password="" -e "use Chinook; show tables;"
'

#my ip